# README #

This is a web based [Leap Motion](https://store-us.leapmotion.com/products/leap-motion-controller) interface for a list of web links.
[![Leap Motion Menu Interface](http://consultingjoe.com/wp-content/uploads/2017/09/LeapMotionMenuInterface.png)](https://www.youtube.com/watch?v=9ameZGE4zsA)

### What is this repository for? ###

* You must have a Leap Motion software/driver installed to use this web page and Javascript API
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* No setup needed
* Run locally in your browser
* Make sure the leap motion visualizer works to know your connected or the icon Leap Motion is green.
* You may have to allow browser/JavaScript access from your Leap Motion settings (Latest Mac install worked right away)

### Contribution guidelines ###

* Writing tests *(Ha, Don't worry)*
* Code review
* **I'm welcome to improvements and new features**

### Who do I talk to? ###

* Me (ConsultingJoe aka Joseph Sammarco)
* [http://ConsultingJoe.com](http://ConsultingJoe.com)
* [Joe@ConsultingJoe.com](mailto:Joe@ConsultingJoe.com?subject=I Like Your Leap Menu Interface)